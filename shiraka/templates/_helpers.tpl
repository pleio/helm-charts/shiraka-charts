{{/*
The name of the release, used as label or a base for other names.
*/}}
{{- define "shiraka.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing media files, i.e. images and documents.
*/}}
{{- define "shiraka.mediaSharedStoragePvcName" -}}
{{- printf "%s-media" (include "shiraka.name" . ) -}}
{{- end }}

{{/*
A set of mutable selector labels to be used for label selectors on Deployments and Services.
*/}}
{{- define "shiraka.selectorLabels" -}}
app.kubernetes.io/name: {{ include "shiraka.name" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}	
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
A set of standard labels as defined by the Helm developers to use in a chart.
*/}}
{{- define "shiraka.standardLabels" -}}
{{ include "shiraka.selectorLabels" . }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
{{- end }}
