# shiraka

![Version: 0.1.5](https://img.shields.io/badge/Version-0.1.5-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for the Shiraka application stack to be run on Kubernetes

## Source Code

* <https://gitlab.com/pleio/helm-charts/shiraka-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| appDebug | string | `nil` | A boolean value whether to run the application in Debug mode or not. |
| appEnv | string | `nil` | The "environment" the application will run in. |
| appName | string | `nil` | The application name as used by Laravel to set the 'name' in config/app.php. |
| appUrl | string | `nil` | The root url of the application, used by the console to properly generate URLs when using the Artisan command line tool. |
| domain | string | `nil` | The domainname to use for the web application Ingress. |
| imagePullSecret | string | `nil` | The secret used to pull the container image from a private container registry. |
| imageRepository | string | `nil` | The repository to pull the image to deploy for the web application. |
| imageTag | string | `nil` | The tag of the image to deploy. |
| livenessProbe.failureThreshold | int | `6` | Number of failed liveness probes before container is restarted |
| livenessProbe.initialDelaySeconds | int | `40` | Number of seconds to delay liveness probe start after container startup |
| livenessProbe.periodSeconds | int | `10` | Interval (in seconds) between liveness probes |
| livenessProbe.successThreshold | int | `1` | Number of successful probes required before the container is marked as live |
| livenessProbe.timeoutSeconds | int | `2` | Timeout in seconds for each liveness probe response |
| logChannel | string | `nil` | The name of the log channel used by Laravel found in config/logging.php. |
| mediaSharedStorageSize | string | `nil` | The size of the RWX PVC used by the web application to store media related files like images etc. |
| podSecurityContext | object | `{}` |  |
| readinessProbe.failureThreshold | int | `6` | Number of failed readiness probes before container is marked as not ready |
| readinessProbe.initialDelaySeconds | int | `20` | Number of seconds to delay readiness probe start after container startup |
| readinessProbe.periodSeconds | int | `10` | Interval (in seconds) between readiness probes |
| readinessProbe.successThreshold | int | `1` | Number of successful probes required before the container is marked as ready |
| readinessProbe.timeoutSeconds | int | `2` | Timeout in seconds for each readiness probe response |
| replicaCount | string | `nil` | The number of replicas to run for the web deployment pod. |
| resources | string | `nil` | The Kubernetes CPU and memory limits and requests to be set to the web application pod. |
| securityContext | object | `{}` |  |
| service.port | int | `8080` |  |
| sharedStorageClassName | string | `nil` | The name of the Storage Class Name as present on the target cluster to use a the storage class for the media storage PVC. |
| useLetsEncryptCertificate | string | `nil` | A toggle to use a LetsEncrypt certificate instead of a certificate provided through a Secret in the cluster. |
| wildcardTlsSecretName | string | `nil` | The name of a cluster Secret containing a (wildcard) certificate to enable TLS for the Ingress. |
